# Spam Client

## Example Usage

spammer spam --type create --duration 30 --rate 10 --query 20 --monitor https://1.1.1.1:3000/api/report  
spammer spam -t create -d 300 -r 10 -q 20 -m https://1.1.1.1:3000/api/report

spammer spam --type transfer --duration 300 --rate 10 --nodelist [ip_1] [ip_2] [ip_3] [ip_4]
spammer spam -t transfer -d 300 -r 10 -n [ip_1] [ip_2] [ip_3] [ip_4]

spammer spam --type create --duration 30 --rate 10 --query 20 --accounts 1000 --monitor https://1.1.1.1:3000/api/report

```
parameter --query or -q is for re-querying active node list atthe given time
```

### Spamming from a file

spammer spam --type create --file rate.txt --monitor http://1.1.1.1:3000/api/report

`rate.txt` file is a file which describes spam duration, rate and accounts for each interval. For example, a file which defines the spam rate can be written like this:

```
loop no
  duration,start tps,end tps,accounts
  wait second
end
```

Example

```
loop 4
  10,5,20,200
end
loop 2
  wait 10
  10,20,10,200
end
```

Above rate file will spam:

- For 10s, starting from 5 tps to 20 tps by creating 200 accounts. this will repeat for 4 times.
- Another loop for 2 times by waiting for 10s, and then for 10s with 20 tps to 10 tps by creating 200 accounts.