#!/usr/bin/env node
"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const crypto = __importStar(require("shardus-crypto-utils"));
const url_1 = require("url");
const helpers_1 = require("yargs/helpers");
const yargs_1 = __importDefault(require("yargs/yargs"));
crypto.init('69fa4195670576c0160d660c3be36556ff8d504725be8a59b5a96509e0c994bc');
const network = '0'.repeat(64);
/*

spammer spam --type create --duration 30 --rate 10 --query 20 --monitor https://1.1.1.1:3000/api/report
spammer spam -t create -d 300 -r 10 -q 20 -m https://1.1.1.1:3000/api/report

spammer spam --type transfer --duration 300 --rate 10 --nodelist [ip_1] [ip_2] [ip_3] [ip_4]
spammer spam -t transfer -d 300 -r 10 -n [ip_1] [ip_2] [ip_3] [ip_4]
 */
const spamTestDir = '.';
let monitorUrl;
async function startSpamSection(argv) {
    const transactionCount = Math.ceil(argv.duration * argv.rate);
    let accountsCount = argv.accounts;
    if (!accountsCount) {
        accountsCount = Math.ceil(argv.duration * argv.rate);
    }
    let accounts = createAccounts(accountsCount);
    if (argv.input) {
        try {
            accounts = JSON.parse(fs_1.default.readFileSync(argv.input, 'utf8'));
            console.log(`Loaded ${accounts.length} account${accounts.length > 1 ? 's' : ''} from ${argv.input}`);
        }
        catch (error) {
            console.log(`Couldn't load accounts from file: ${error.message}`);
        }
    }
    if (argv.output) {
        try {
            fs_1.default.writeFileSync(argv.output, JSON.stringify(accounts, null, 2));
            console.log(`Wrote ${accounts.length} account${accounts.length > 1 ? 's' : ''} to ${argv.output}`);
        }
        catch (error) {
            console.log(`Couldn't write accounts to file: ${error.message}`);
        }
    }
    const txs = makeTxGenerator(accounts, transactionCount, argv.type);
    const queryTime = argv.query;
    monitorUrl = argv.monitor;
    let nodes;
    if (argv.nodelist) {
        nodes = argv.nodelist;
    }
    else {
        nodes = await getSeedNodes(argv.monitor);
    }
    await spamTxs({
        verbose: argv.verbose,
        txs,
        count: transactionCount,
        rate: argv.rate,
        nodes,
        saveFile: 'spam-test.json',
        queryTime,
    });
    console.log('Done spamming...');
}
function fetchNthItemFromLine(n, line, delimiter = ',') {
    let item = line.split(delimiter)[n];
    item = item.trim();
    if (!Number.isNaN(parseInt(item)))
        return parseInt(item);
    else
        return item;
}
async function runLinesInLoop(lines, loopCount, argv) {
    if (lines.length === 0)
        return;
    let i = 0;
    console.log('loopCount', loopCount);
    console.log('collected lines', lines);
    if (loopCount === 'forever')
        loopCount = 10e6;
    while (i < loopCount) {
        console.log('looping', i + 1);
        for (const line of lines) {
            if (fetchNthItemFromLine(0, line, ' ') === 'wait') {
                const waitTime = fetchNthItemFromLine(1, line, ' ');
                console.log('Waiting for', waitTime * 1000, 'ms');
                await _sleep(waitTime * 1000);
                continue;
            }
            const duration = fetchNthItemFromLine(0, line);
            const startTps = fetchNthItemFromLine(1, line);
            const endTps = fetchNthItemFromLine(2, line);
            const accounts = fetchNthItemFromLine(3, line);
            const tpsIncrement = (endTps - startTps) / (duration + 1);
            for (let i = 0; i < duration; i++) {
                const tps = startTps + i * tpsIncrement;
                argv.duration = 1;
                argv.rate = tps;
                argv.accounts = accounts;
                await startSpamSection(argv);
            }
        }
        i += 1;
    }
}
yargs_1.default(helpers_1.hideBin(process.argv))
    .command('spam', 'spam nodes from [monitor] using [type] transaction for [duration] seconds at [rate] tps', () => { }, async (argv) => {
    if (!argv.file)
        await startSpamSection(argv);
    else if (argv.file) {
        const data = fs_1.default.readFileSync(argv.file, 'utf8');
        const lines = data.split(/\r?\n/);
        let collectedLines = [];
        let loopCount = 1;
        for (let line of lines) {
            if (!line)
                return;
            line = line.trim();
            if (fetchNthItemFromLine(0, line, ' ') === 'loop') {
                loopCount = fetchNthItemFromLine(1, line, ' ');
                continue;
            }
            if (fetchNthItemFromLine(0, line, ' ') === 'end') {
                await runLinesInLoop(collectedLines, loopCount, argv);
                collectedLines = [];
                continue;
            }
            collectedLines.push(line);
        }
        await runLinesInLoop(collectedLines, loopCount, argv);
    }
})
    .option('type', {
    alias: 't',
    type: 'string',
    description: 'The transaction type to spam the network with',
})
    .option('duration', {
    alias: 'd',
    type: 'number',
    description: 'The duration (in seconds) to spam the network',
})
    .option('file', {
    alias: 'f',
    type: 'string',
    description: 'Filename which define spam rate and count',
})
    .option('rate', {
    alias: 'r',
    type: 'number',
    description: 'The rate (in tps) to spam the network at',
})
    .option('query', {
    alias: 'q',
    type: 'number',
    description: 'The second to re-query the nodelist from the server',
})
    .option('monitor', {
    alias: 'm',
    type: 'string',
    description: 'The monitor report url',
})
    .option('nodelist', {
    alias: 'n',
    type: 'array',
    description: 'A list of ip addresses of the nodes to spam',
})
    .option('verbose', {
    alias: 'v',
    type: 'boolean',
    description: 'Prints txs sent in 10s',
})
    .option('accounts', {
    alias: 'a',
    type: 'number',
    description: 'The number of accounts to use for the test',
})
    .option('input', {
    alias: 'i',
    type: 'string',
    description: 'Reuse accounts from given file',
})
    .option('output', {
    alias: 'o',
    type: 'string',
    description: 'Save accounts to given file for reuse',
}).argv;
async function getSeedNodes(monitorUrl) {
    const { data } = await axios_1.default.get(`${monitorUrl}`); // await utils.getJson(`${glob.seedNode}/nodelist`)
    const report = data;
    // Parse report into nodelist of IPs
    // Just active nodes
    // const nodelist = Object.values(report.nodes['active']).map(
    //   info => `${info.nodeIpInfo.externalIp}:${info.nodeIpInfo.externalPort}`
    // )
    /**
     * If the monitorUrl IP is not ['localhost', '127.0.0.1', '0.0.0.0']
     * and the nodeIpInfo.externalIp is one of the above,
     *
     * Then replace the nodeIpInfo.externalIp with the monitorUrl IP
     */
    function isLocalHost(host) {
        const localHosts = ['localhost', '127.0.0.1', '0.0.0.0'];
        return localHosts.includes(host);
    }
    const monitorHost = new url_1.URL(monitorUrl).hostname;
    const nodelist = Object.values(report.nodes['active']).map(info => {
        let nodeHost;
        try {
            nodeHost = info.nodeIpInfo.externalIp;
        }
        catch (error) {
            console.log(`Couldn't access info.nodeIpInfo.externalIp: ${error.message}`);
            return '';
        }
        if (!isLocalHost(monitorHost) && isLocalHost(nodeHost))
            nodeHost = monitorHost;
        return `${nodeHost}:${info.nodeIpInfo.externalPort}`;
    });
    // All joining, syncing, and active nodes
    /*
    const nodelist = Object.values(report.nodes).reduce(
      (accumulator: string[], status) =>
        accumulator.concat(
          Object.values(status).map(
            info =>
              `${info.nodeIpInfo.externalIp}:${info.nodeIpInfo.externalPort}`
          )
        ),
      []
    )
     */
    return nodelist;
}
function createAccount(keys = crypto.generateKeypair()) {
    return {
        address: keys.publicKey,
        keys,
    };
}
function createAccounts(num) {
    const accounts = new Array(num).fill(0).map(() => createAccount());
    return accounts;
}
function makeTxGenerator(accounts, total, type) {
    function* buildGenerator(txBuilder, accounts, total, type) {
        let account1, offset, account2;
        while (total > 0) {
            // Keep looping through all available accounts as the srcAcct
            account1 = accounts[total % accounts.length];
            // Pick some other random account as the tgtAcct
            offset = Math.floor(Math.random() * (accounts.length - 1)) + 1;
            account2 = accounts[(total + offset) % accounts.length];
            // // Return a create tx to add funds to the srcAcct
            // if (type === 'transfer') {
            //   yield txBuilder({
            //     type: 'create',
            //     from: account1,
            //     to: account1,
            //     amount: 1,
            //   })
            //   total--
            //   if (!(total > 0)) break
            // }
            // Return a transfer tx to transfer funds from srcAcct to tgtAcct
            switch (type) {
                case 'create': {
                    yield txBuilder({
                        type: 'create',
                        from: account1,
                        to: account1,
                        amount: 1,
                    });
                    break;
                }
                case 'debug-create': {
                    yield txBuilder({
                        type: 'debug-create',
                        from: account1,
                        to: account1,
                        amount: 1,
                    });
                    break;
                }
                case 'transfer': {
                    yield txBuilder({
                        type: 'transfer',
                        from: account1,
                        to: account2,
                        amount: 1,
                    });
                    break;
                }
                default: {
                    console.log('Type must be `create`, `debug-create` or `transfer`');
                }
            }
            total--;
            if (!(total > 0))
                break;
        }
    }
    const generator = buildGenerator(buildTx, accounts, total, type);
    return generator;
}
function buildTx({ type, from, to, amount }) {
    let actualTx;
    switch (type) {
        case 'create': {
            actualTx = {
                type,
                from: '0'.repeat(64),
                to: from.address,
                amount: Number(amount),
                timestamp: Date.now(),
            };
            break;
        }
        case 'debug-create': {
            actualTx = {
                type,
                from: from.address,
                to: from.address,
                amount: Number(amount),
                timestamp: Date.now(),
            };
            break;
        }
        case 'transfer': {
            actualTx = {
                type,
                network,
                from: from.address,
                timestamp: Date.now(),
                to: to.address,
                amount: Number(amount),
            };
            break;
        }
        default: {
            console.log('Type must be `create`, `debug-create` or `transfer``');
        }
    }
    if (from.keys) {
        crypto.signObj(actualTx, from.keys.secretKey, from.keys.publicKey);
    }
    else {
        crypto.signObj(actualTx, to.keys.secretKey, to.keys.publicKey);
    }
    return actualTx;
}
async function sendTx(tx, node = null, _verbose = true) {
    // console.log(node);
    if (!tx.sign) {
        tx = buildTx(tx);
    }
    // if (verbose) {
    //   console.log(`Sending tx to ${node}...`)
    //   console.log(tx)
    // }
    try {
        // console.log(target, node)
        const { data } = await axios_1.default.post(`http://${node}/inject`, tx);
        if (data && data.result && data.result.success === true)
            return true;
        console.log('Got false response:', data);
        return false;
    }
    catch (err) {
        console.log(`Node ${node} did not respond to the the request`);
        return false;
    }
}
async function spamTxs({ verbose, txs, count, rate, nodes = [], saveFile = null, queryTime, }) {
    if (!Array.isArray(nodes))
        nodes = [nodes];
    console.log(`Spamming ${nodes.length > 1 ? 'nodes' : 'node'} ${nodes.join()} with ${count} txs at ${rate} TPS...`);
    const writeStream = saveFile
        ? fs_1.default.createWriteStream(path_1.default.join(spamTestDir, saveFile))
        : null;
    let node;
    let successful = 0;
    let unsuccessful = 0;
    console.log('awaiting...');
    // eslint-disable-next-line no-async-promise-executor
    await new Promise(async (resolve) => {
        let counter = 0;
        if (queryTime) {
            // eslint-disable-next-line no-var
            var queryInterval = setInterval(async () => {
                nodes = await getSeedNodes(monitorUrl);
            }, queryTime * 1000);
        }
        // eslint-disable-next-line no-constant-condition
        while (true) {
            const tx = txs.next().value;
            if (!tx) {
                if (queryTime) {
                    // eslint-disable-next-line block-scoped-var
                    clearInterval(queryInterval);
                }
                break;
            }
            const index = Math.floor(Math.random() * nodes.length);
            node = nodes[index];
            sendTx(tx, node)
                .then(success => {
                if (!success) {
                    nodes.splice(index, 1);
                    unsuccessful++;
                }
                else {
                    if (writeStream)
                        writeStream.write(JSON.stringify(tx, null, 2) + '\n');
                    successful++;
                }
            })
                .catch(() => {
                nodes.splice(index, 1);
                unsuccessful++;
            })
                .finally(() => {
                counter++;
                if (verbose) {
                    if (counter % 10 === 0) {
                        process.stdout.clearLine(0);
                        process.stdout.cursorTo(0);
                        process.stdout.write(counter + '');
                    }
                }
                if (counter >= count) {
                    if (verbose) {
                        process.stdout.write('\n');
                    }
                    resolve();
                }
            });
            await _sleep((1 / rate) * 1000);
        }
    });
    console.log('done');
    if (writeStream)
        writeStream.end();
    console.log();
    console.log(`Done spamming. There were ${successful} successful transactions injected into the network and ${unsuccessful} unsuccessful transactions that were rejected because the node was no longer in the network.`);
    if (writeStream) {
        await new Promise(resolve => writeStream.on('finish', resolve));
        console.log(`Wrote spammed txs to '${saveFile}'`);
    }
}
async function _sleep(ms = 0) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
//# sourceMappingURL=index.js.map